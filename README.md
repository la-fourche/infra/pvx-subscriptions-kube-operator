# PVX Subscriptions Kube Operator

This Kubernetes Operator can create, update, and delete PeopleVox subscriptions, using a Kubernetes CustomResourceDefinition, to ease the deployment and fulfill the lack of user interface or even list method in the PVX Subscriptions API.

## Getting Started

First of all, you need to install the CRD and this operator in your kubernetes cluster.

You can do it easily using the Helm Chart provided here (TODO).

## RBAC

The Helm Chart automatically create a Service Account with a ClusterRole allowing the Operator to read any secret in your cluster. To prevent that, you can provide a custom Service Account associated with a custom Role or ClusterRole, depending on your company security policies.

The Service Account needs the following rights:

* *get*, *list*, *watch* and *update* on the **pvxsubscriptions** of the **stable.lafourche.fr**, in the namespaces you will use, or cluster wide
* *get* on the **secrets** of the core library, in the namespaces you will use, or cluster wide

## Example PVXSubscription

Here is an example of a PVX subscription managed by this Operator:

```yaml
apiVersion: stable.lafourche.fr/v1
kind: PVXSubscription
metadata:
  name: example
  namespace: default
spec:
  method: GET
  event: AvailabilityChanges
  filter: ''
  url: https://test-url.example.com
  encodeParamsData: true
  preventDelete: false
  params:
  - name: item
    attribute: ItemCode
  - name: available
    attribute: Available
  config:
    wsdl: https://qac.peoplevox.net/yourclientid9999/resources/integrationservicev4.asmx?wsdl
    auth:
      clientId: yourclientid9999
      username: yourUsername
      passwordSecret:
        secretName: pvx-password-secret
        secretKey: password
        secretNamespace: default
```

With a Secret like:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: pvx-password-secret
  namespace: default
data:
  password: YOURBASE64SECRET
type: Opaque
```

> Be careful, PVX needs a base64 encoded password to be sent for the API authentication, in the Kubernetes secret you must base64 encode the base64 encoded password => ```base64(base64(actualPassword))```

### Prevent Delete

The `preventDelete` flag can protect your production PVX subscriptions. It will not prevent the Kubernetes resource to be deleted, but your actual PVX subscription will remain untouched.

## Kubectl usage

You can use the CRD as any other resources, the short name is ```pvxs```.

Examples:

```bash
kubectl get pvxs
kubectl get pvxs test -n my-namespace -o yaml
kubectl delete pvxs test -n my-namespace
```

Enjoy!