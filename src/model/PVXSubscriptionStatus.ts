export class PVXSubscriptionStatus {
  pvxSubscriptionId: number;
  encodedSpecs: string;
}
