import { Secret } from './Secret';

export class PVXAuth {
  clientId: string;
  username: string;
  passwordSecret: Secret;
}
