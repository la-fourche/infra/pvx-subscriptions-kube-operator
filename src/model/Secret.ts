export class Secret {
  name: string;
  key: string;
  namespace: string;
}
