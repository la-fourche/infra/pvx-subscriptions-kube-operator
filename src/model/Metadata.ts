export class Metadata {
  annotations: Map<string, string>;
  creationTimestamp: Date;
  generation: number;
  name: string;
  namespace: string;
  resourceVersion: string;
  selfLink: string;
  uid: string;
}
