export enum Method {
  GET = 'GET',
  POST = 'POST',
  GET_SITE_FILTER = 'GET_SITE_FILTER',
}
