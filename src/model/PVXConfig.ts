import { PVXAuth } from './PVXAuth';

export class PVXConfig {
  wsdl: string;
  auth: PVXAuth;
}
