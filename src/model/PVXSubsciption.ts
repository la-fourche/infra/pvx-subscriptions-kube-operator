import { Metadata } from './Metadata';
import { PVXSubscriptionSpecs } from './PVXSubscriptionSpecs';
import { PVXSubscriptionStatus } from './PVXSubscriptionStatus';

export class PVXSubscription {
  apiVersion: string;
  kind: string;
  metadata: Metadata;
  spec: PVXSubscriptionSpecs;
  status: PVXSubscriptionStatus;
}
