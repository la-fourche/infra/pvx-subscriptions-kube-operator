import { PVXPostParam } from './PVXPostParam';
import { PVXConfig } from './PVXConfig';
import { Method } from './Method';

export class PVXSubscriptionSpecs {
  encodeParamsData: boolean;
  preventDelete: boolean;
  event: string;
  filter: string;
  siteFilter: string;
  method: Method;
  url: string;
  params: Array<PVXPostParam>;
  config: PVXConfig;
}
