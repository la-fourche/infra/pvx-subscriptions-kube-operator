import Operator, { ResourceEventType } from '@dot-i/k8s-operator';
import { ObjectToPVXSubscriptionTransformer } from './transformers/ObjectToPVXSubscriptionTransformer';
import { Logger } from 'winston';
import { Injectable, Inject } from 'injection-js';
import { APP_LOGGER } from './di/logger';
import { CreateSubscriptionHandler } from './handlers/CreateSubscriptionHandler';
import { UpdateSubscriptionHandler } from './handlers/UpdateSubscriptionHandler';
import { DeleteSubscriptionHandler } from './handlers/DeleteSubscriptionHandler';

@Injectable()
export default class PVXSubscriptionsOperator extends Operator {
  constructor(
    @Inject(APP_LOGGER) private logger: Logger,
    private subscriptionTransformer: ObjectToPVXSubscriptionTransformer,
    private createSubscriptionHandler: CreateSubscriptionHandler,
    private deleteSubscriptionHandler: DeleteSubscriptionHandler,
    private updateSubscriptionHandler: UpdateSubscriptionHandler,
  ) {
    super(logger);
  }

  protected async init() {
    await this.watchResource(
      'stable.lafourche.fr',
      'v1',
      'pvxsubscriptions',
      async (e) => {
        const subscription = this.subscriptionTransformer.transform(e.object);

        switch (e.type) {
          case ResourceEventType.Added:
            this.createSubscriptionHandler.handle(subscription);
            break;
          case ResourceEventType.Modified:
            this.updateSubscriptionHandler.handle(subscription);
            break;
          case ResourceEventType.Deleted:
            this.deleteSubscriptionHandler.handle(subscription);
            break;
        }
      },
    );
  }
}
