import { InjectionToken } from 'injection-js';
import { Logger, createLogger, format, transports } from 'winston';

export const logger = createLogger({
  level: 'info',
  exitOnError: false,
  format: format.json(),
  transports: [new transports.Console()],
});

export const APP_LOGGER = new InjectionToken<Logger>('app.logger');
