import { KubeConfig, CustomObjectsApi } from '@kubernetes/client-node';

const kc = new KubeConfig();
kc.loadFromDefault();
export const kubernetesClient = kc.makeApiClient(CustomObjectsApi);
