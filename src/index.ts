import 'reflect-metadata';
import { ReflectiveInjector } from 'injection-js';

import PVXSubscriptionsOperator from './PVXSubscriptionOperator';
import { ObjectToPVXSubscriptionSpecsTransformer } from './transformers/ObjectToPVXSubscriptionSpecsTransformer';
import { ObjectToPVXSubscriptionTransformer } from './transformers/ObjectToPVXSubscriptionTransformer';
import { PVXSubscriptionSpecsToEncodedSpecs } from './transformers/PVXSubscriptionSpecsToEncodedSpecs';
import { logger, APP_LOGGER } from './di/logger';
import { CustomObjectsApi } from '@kubernetes/client-node';
import { CreateSubscriptionHandler } from './handlers/CreateSubscriptionHandler';
import { DeleteSubscriptionHandler } from './handlers/DeleteSubscriptionHandler';
import { UpdateSubscriptionHandler } from './handlers/UpdateSubscriptionHandler';
import { kubernetesClient } from './di/KubernetesCRDClient';

const injector = ReflectiveInjector.resolveAndCreate([
  ObjectToPVXSubscriptionSpecsTransformer,
  ObjectToPVXSubscriptionTransformer,
  PVXSubscriptionSpecsToEncodedSpecs,
  PVXSubscriptionsOperator,
  CreateSubscriptionHandler,
  DeleteSubscriptionHandler,
  UpdateSubscriptionHandler,
  { provide: APP_LOGGER, useValue: logger },
  { provide: CustomObjectsApi, useValue: kubernetesClient },
]);

const main = async () => {
  const operator = injector.get(PVXSubscriptionsOperator);
  await operator.start();

  const exit = () => {
    operator.stop();
    process.exit(0);
  };

  process.on('SIGTERM', () => exit()).on('SIGINT', () => exit());
};

main();
