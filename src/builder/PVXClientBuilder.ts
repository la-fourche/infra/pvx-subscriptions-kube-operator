import { Secret } from '../model/Secret';
import { CoreV1Api, KubeConfig, V1Secret } from '@kubernetes/client-node';
import { PvxClient } from '@lafourche/pvx-node-client';

export class PVXClientBuilder {
  private kubeClient: CoreV1Api;
  private wsdl: string;
  private clientId: string;
  private username: string;
  private password: string;

  constructor() {
    const kc = new KubeConfig();
    kc.loadFromDefault();
    this.kubeClient = kc.makeApiClient(CoreV1Api);
  }

  public withWSDL(wsdl: string) {
    this.wsdl = wsdl;
  }

  public withClientId(clientId: string) {
    this.clientId = clientId;
  }

  public withUsername(username: string) {
    this.username = username;
  }

  public withPassword(password: string) {
    this.password = password;
  }

  public async withKubeSecretPassword(secret: Secret) {
    const kubeSecret = await this.kubeClient.readNamespacedSecret(
      secret.name,
      secret.namespace,
    );
    const ksecret: V1Secret = kubeSecret.body;
    if (ksecret.data && ksecret.data[secret.key]) {
      this.password = Buffer.from(ksecret.data[secret.key], 'base64').toString(
        'ascii',
      );
    }
  }

  public build(): PvxClient {
    return new PvxClient(
      {
        clientId: this.clientId,
        username: this.username,
        password: this.password,
      },
      this.wsdl,
    );
  }
}
