import { PVXPostParam } from '../model/PVXPostParam';

export class PVXParamsStringBuilder {
  private params: Array<PVXPostParam>;

  constructor() {
    this.params = [];
  }

  public withParam(param: PVXPostParam) {
    this.params.push(param);
  }

  public withParams(params: Array<PVXPostParam>) {
    this.params.push(...params);
  }

  public build(glue = '&'): string {
    return this.params
      .map((param) => `${param.name}={${param.attribute}}`)
      .join(glue);
  }
}
