import { Injectable, Inject } from 'injection-js';
import { PVXSubscription } from '../model/PVXSubsciption';
import { SubscriptionHandler } from './SubscriptionHandler';
import { EventType } from '@lafourche/pvx-node-client';
import { APP_LOGGER } from '../di/logger';
import { Logger } from 'winston';
import { PVXSubscriptionSpecsToEncodedSpecs } from '../transformers/PVXSubscriptionSpecsToEncodedSpecs';
import { CustomObjectsApi } from '@kubernetes/client-node';
import { Method } from '../model/Method';

@Injectable()
export class CreateSubscriptionHandler extends SubscriptionHandler {
  constructor(
    @Inject(APP_LOGGER) protected logger: Logger,
    private kubernetesClient: CustomObjectsApi,
    private specsToEncodedSpecsTransformer: PVXSubscriptionSpecsToEncodedSpecs,
  ) {
    super();
  }

  public async handle(subscription: PVXSubscription) {
    if (typeof subscription.status.pvxSubscriptionId != 'undefined') {
      this.logger.info('Subscription already exist.', {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: subscription.status.pvxSubscriptionId,
      });

      return;
    }

    try {
      const pvx = await this.createPvxClient(subscription);

      this.logger.info('Creating subscription...', {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
      });

      const subscriptionMethods = {
        [Method.GET]: async () => {
          return await pvx.subscribeEvent(
            subscription.spec.event as EventType,
            this.createCallBackUrl(
              subscription.spec.url,
              subscription.spec.params,
            ),
            subscription.spec.filter,
            subscription.spec.encodeParamsData,
          );
        },
        [Method.POST]: async () => {
          return await pvx.subscribePostEvent(
            subscription.spec.event as EventType,
            subscription.spec.url,
            this.createPVXPostParams(subscription.spec.params),
            subscription.spec.filter,
            subscription.spec.encodeParamsData,
          );
        },
        [Method.GET_SITE_FILTER]: async () => {
          return await pvx.subscribeEventWithSitesFilters(
            subscription.spec.event as EventType,
            this.createCallBackUrl(
              subscription.spec.url,
              subscription.spec.params,
            ),
            subscription.spec.siteFilter,
            subscription.spec.filter,
            subscription.spec.encodeParamsData,
          );
        },
      };

      if (!(subscription.spec.method in subscriptionMethods)) {
        throw new Error('Method not supported');
      }

      const pvxSubscriptionId = parseInt(
        await subscriptionMethods[subscription.spec.method](),
      );

      this.logger.info(
        `Subscription has been assigned the id ${pvxSubscriptionId}.`,
        {
          subscription_name: subscription.metadata.name,
          subscription_namespace: subscription.metadata.namespace,
          subscription_pvx_id: pvxSubscriptionId,
        },
      );

      subscription.status.pvxSubscriptionId = pvxSubscriptionId;
      subscription.status.encodedSpecs = this.specsToEncodedSpecsTransformer.transform(
        subscription.spec,
      );

      this.logger.info(`Subscription status edited.`, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: pvxSubscriptionId,
      });

      await this.kubernetesClient.replaceNamespacedCustomObjectStatus(
        'stable.lafourche.fr',
        'v1',
        subscription.metadata.namespace,
        'pvxsubscriptions',
        subscription.metadata.name,
        subscription,
      );

      this.logger.info(`Subscription has been successfully created.`, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: pvxSubscriptionId,
      });
    } catch (e) {
      this.logger.error(e, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
      });
    }
  }
}
