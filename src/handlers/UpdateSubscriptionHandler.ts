import { Injectable, Inject } from 'injection-js';
import { PVXSubscription } from '../model/PVXSubsciption';
import { SubscriptionHandler } from './SubscriptionHandler';
import { APP_LOGGER } from '../di/logger';
import { Logger } from 'winston';
import { PVXSubscriptionSpecsToEncodedSpecs } from '../transformers/PVXSubscriptionSpecsToEncodedSpecs';
import { CreateSubscriptionHandler } from './CreateSubscriptionHandler';
import { DeleteSubscriptionHandler } from './DeleteSubscriptionHandler';
import { PVXSubscriptionStatus } from '../model/PVXSubscriptionStatus';

@Injectable()
export class UpdateSubscriptionHandler extends SubscriptionHandler {
  constructor(
    @Inject(APP_LOGGER) protected logger: Logger,
    private specsToEncodedSpecsTransformer: PVXSubscriptionSpecsToEncodedSpecs,
    private createSubscriptionHandler: CreateSubscriptionHandler,
    private deleteSubscriptionHandler: DeleteSubscriptionHandler,
  ) {
    super();
  }

  public async handle(subscription: PVXSubscription) {
    const encodedNewSpecs = this.specsToEncodedSpecsTransformer.transform(
      subscription.spec,
    );

    if (encodedNewSpecs == subscription.status.encodedSpecs) {
      this.logger.info(`Subscription specs have not changed.`, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: subscription.status.pvxSubscriptionId,
      });

      return;
    }

    this.logger.info(`Updating subscription...`, {
      subscription_name: subscription.metadata.name,
      subscription_namespace: subscription.metadata.namespace,
      subscription_pvx_id: subscription.status.pvxSubscriptionId,
    });

    if (subscription.spec.preventDelete) {
      this.logger.warn(`This subscription is protected by a prevent delete spec. Updating requires deleting.`, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: subscription.status.pvxSubscriptionId,
      });
      subscription.spec.preventDelete = false;
      await this.deleteSubscriptionHandler.handle(subscription);
      subscription.spec.preventDelete = true;
    } else {
      await this.deleteSubscriptionHandler.handle(subscription);
    }

    subscription.status = new PVXSubscriptionStatus();
    await this.createSubscriptionHandler.handle(subscription);

    this.logger.info(`Subscription successfully updated.`, {
      subscription_name: subscription.metadata.name,
      subscription_namespace: subscription.metadata.namespace,
      subscription_pvx_id: subscription.status.pvxSubscriptionId,
    });
  }
}
