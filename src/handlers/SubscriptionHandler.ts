import { Injectable } from 'injection-js';
import { PVXSubscription } from '../model/PVXSubsciption';
import { PVXClientBuilder } from '../builder/PVXClientBuilder';
import { PVXPostParam } from '../model/PVXPostParam';
import { PVXParamsStringBuilder } from '../builder/PVXParamsStringBuilder';

@Injectable()
export abstract class SubscriptionHandler {
  public abstract handle(subscription: PVXSubscription);

  protected async createPvxClient(subscription: PVXSubscription) {
    const builder = new PVXClientBuilder();

    builder.withWSDL(subscription.spec.config.wsdl);
    builder.withClientId(subscription.spec.config.auth.clientId);
    builder.withUsername(subscription.spec.config.auth.username);
    await builder.withKubeSecretPassword(
      subscription.spec.config.auth.passwordSecret,
    );

    return builder.build();
  }

  protected createPVXPostParams(params: Array<PVXPostParam>) {
    const builder = new PVXParamsStringBuilder();
    builder.withParams(params);

    return builder.build();
  }

  protected createCallBackUrl(url: string, params: Array<PVXPostParam>) {
    if (params.length === 0) {
      return url;
    }

    let concatWith = '?';
    if (url.indexOf('?') !== -1) {
      concatWith = '&';
    }

    return `${url}${concatWith}${this.createPVXPostParams(params)}`;
  }
}
