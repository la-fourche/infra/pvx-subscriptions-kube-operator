import { Injectable, Inject } from 'injection-js';
import { PVXSubscription } from '../model/PVXSubsciption';
import { SubscriptionHandler } from './SubscriptionHandler';
import { APP_LOGGER } from '../di/logger';
import { Logger } from 'winston';

@Injectable()
export class DeleteSubscriptionHandler extends SubscriptionHandler {
  constructor(@Inject(APP_LOGGER) protected logger: Logger) {
    super();
  }

  public async handle(subscription: PVXSubscription) {
    if (typeof subscription.status.pvxSubscriptionId == 'undefined') {
      this.logger.warn(`Subscription does not exist in PVX.`, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
      });

      return;
    }

    if (subscription.spec.preventDelete) {
      this.logger.warn(`Subscription protected by a prevent delete spec.`, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: subscription.status.pvxSubscriptionId,
      });

      return;
    }

    try {
      this.logger.info(`Deleting subscription...`, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: subscription.status.pvxSubscriptionId,
      });

      const pvx = await this.createPvxClient(subscription);
      const pvxResponse = await pvx.unsubscribeEvent(
        subscription.status.pvxSubscriptionId,
      );

      if (pvxResponse === 'True') {
        this.logger.info(`Subscription successfully deleted.`, {
          subscription_name: subscription.metadata.name,
          subscription_namespace: subscription.metadata.namespace,
          subscription_pvx_id: subscription.status.pvxSubscriptionId,
        });
      } else {
        throw `Subscription ${subscription.metadata.name} in namespace ${subscription.metadata.namespace} failed to be deleted, PVX response => ${pvxResponse}.`;
      }
    } catch (e) {
      this.logger.error(e, {
        subscription_name: subscription.metadata.name,
        subscription_namespace: subscription.metadata.namespace,
        subscription_pvx_id: subscription.status.pvxSubscriptionId,
      });
    }
  }
}
