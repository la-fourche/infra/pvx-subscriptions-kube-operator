import { Metadata } from '../model/Metadata';
import { PVXSubscription } from '../model/PVXSubsciption';
import { ObjectToPVXSubscriptionSpecsTransformer } from './ObjectToPVXSubscriptionSpecsTransformer';
import { PVXSubscriptionStatus } from '../model/PVXSubscriptionStatus';
import { Injectable } from 'injection-js';

@Injectable()
export class ObjectToPVXSubscriptionTransformer {
  private specTranformer: ObjectToPVXSubscriptionSpecsTransformer;

  constructor() {
    this.specTranformer = new ObjectToPVXSubscriptionSpecsTransformer();
  }

  /**
   * transform
   */
  public transform(object: any): PVXSubscription {
    const subscription = new PVXSubscription();
    subscription.spec = this.specTranformer.transform(object.spec);
    subscription.metadata = Object.assign(new Metadata(), object.metadata);
    subscription.kind = object.kind;
    subscription.apiVersion = object.apiVersion;
    subscription.status = Object.assign(
      new PVXSubscriptionStatus(),
      object.status,
    );

    return subscription;
  }
}
