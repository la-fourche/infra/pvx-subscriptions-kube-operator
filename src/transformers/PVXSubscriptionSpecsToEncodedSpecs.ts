import { PVXSubscriptionSpecs } from '../model/PVXSubscriptionSpecs';
import { Injectable } from 'injection-js';

@Injectable()
export class PVXSubscriptionSpecsToEncodedSpecs {
  /**
   * transform
   */
  public transform(specs: PVXSubscriptionSpecs): string {
    return Buffer.from(JSON.stringify(specs)).toString('base64');
  }
}
