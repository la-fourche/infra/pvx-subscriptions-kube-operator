import { PVXSubscriptionSpecs } from '../model/PVXSubscriptionSpecs';
import { PVXPostParam } from '../model/PVXPostParam';
import { Secret } from '../model/Secret';
import { PVXAuth } from '../model/PVXAuth';
import { PVXConfig } from '../model/PVXConfig';
import { Injectable } from 'injection-js';

@Injectable()
export class ObjectToPVXSubscriptionSpecsTransformer {
  /**
   * transform
   */
  public transform(object: any): PVXSubscriptionSpecs {
    const passwordSecret = new Secret();
    passwordSecret.key = object.config.auth.passwordSecret.secretKey;
    passwordSecret.namespace =
      object.config.auth.passwordSecret.secretNamespace;
    passwordSecret.name = object.config.auth.passwordSecret.secretName;

    const auth = Object.assign(new PVXAuth(), object.config.auth);
    auth.passwordSecret = passwordSecret;

    const config = Object.assign(new PVXConfig(), object.config);
    config.auth = auth;

    const spec = Object.assign(new PVXSubscriptionSpecs(), object);
    spec.params = spec.params.map((param) =>
      Object.assign(new PVXPostParam(), param),
    );
    spec.config = config;

    return spec;
  }
}
