FROM node:lts as build

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

WORKDIR /app

RUN npm i

COPY . /app

RUN npm run build

FROM node:lts as operator

ENV NODE_ENV=production

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

WORKDIR /app

RUN npm i

COPY --from=build /app/.build /app/.build

CMD ["npm", "start"]
